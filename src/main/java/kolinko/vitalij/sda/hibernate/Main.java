package kolinko.vitalij.sda.hibernate;

import kolinko.vitalij.sda.hibernate.entity.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        // making connection to database
        // hibernate.properties is used instead of Configuration() parameters
        SessionFactory sessionFactory = new Configuration()
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        // create session
        Session session = null;

//        // === CREATE ===
//        session = sessionFactory.getCurrentSession();
//        // open session transaction
//        session.beginTransaction();
//        Student student1 = new Student("Tadas", "tadas@tadas.lt", 30);
//        Student student2 = new Student("Andrius", "andrius@andrius.lt", 24);
//        Student student3 = new Student("Rimas", "rimas@rimas.lt", 29);
//        Student student4 = new Student("Petras", "petras@petras.lt", 50);
//        Student student5 = new Student("Marius", "marius@marius.lt", 39);
//        System.out.println(student1);
//        System.out.println(student2);
//        System.out.println(student3);
//        System.out.println(student4);
//        System.out.println(student5);
//        session.save(student1);
//        session.save(student2);
//        session.save(student3);
//        session.save(student4);
//        session.save(student5);
//        System.out.println(student1);
//        System.out.println(student2);
//        System.out.println(student3);
//        System.out.println(student4);
//        System.out.println(student5);

//        // === READ ===
//        session = sessionFactory.getCurrentSession();
//        // open session transaction
//        session.beginTransaction();
//        Student studentFromDb = session.get(Student.class, 1L);
//        System.out.println(studentFromDb);

//        // === UPDATE ===
//        session = sessionFactory.getCurrentSession();
//        // open session transaction
//        session.beginTransaction();
//        Student studentFromDb = session.get(Student.class, 1L);
//        System.out.println(studentFromDb);
//        studentFromDb.setAge(37);
//
//        // close session transaction
//        session.getTransaction().commit();
//        // === DELETE ===
//        session = sessionFactory.getCurrentSession();
//        session.beginTransaction();
//        Student studentFromDb = session.get(Student.class, 1L);
//        session.remove(studentFromDb);
        // === READ w QUERY
        session = sessionFactory.getCurrentSession();
        session.beginTransaction();

        Student student = session.createQuery("SELECT s FROM Student s WHERE s.id=3", Student.class)
                .getSingleResult();
        System.out.println(student);

        Student student1 = session.createQuery("SELECT s FROM Student s WHERE s.id=:id", Student.class)
                .setParameter("id",  4L)
                .getSingleResult();
        System.out.println(student1);

        List<Student> students = session.createQuery("SELECT s FROM Student s", Student.class)
                .getResultList();
        students.forEach(System.out::println);

        session.getTransaction().commit();
        sessionFactory.close();
    }
}